package ru.tsc.tambovtsev.tm.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.User;

@Getter
@Setter
public class AbstractUserResponse extends AbstractResponse{

    @Nullable
    private User user;

    public AbstractUserResponse() {
    }

    public AbstractUserResponse(@Nullable User user) {
        this.user = user;
    }
}
