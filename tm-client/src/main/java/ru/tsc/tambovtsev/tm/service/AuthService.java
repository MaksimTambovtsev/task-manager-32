package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.service.IAuthService;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.exception.field.PermissionException;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.LoginEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.PasswordEmptyException;
import ru.tsc.tambovtsev.tm.exception.system.AccessDeniedException;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import java.util.Arrays;
import java.util.Optional;


public class AuthService implements IAuthService {

    @NotNull
    private IPropertyService propertyService;

    @Nullable
    private String userId;

    @Override
    public void login(@Nullable final String login,
                      @Nullable final String password) {
    }

    @NotNull
    @Override
    public User getUser() {
        return null;
    }

    @Override
    public @NotNull User check(@Nullable String login, @Nullable String password) {
        return null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    @Nullable
    public User registry(@Nullable final String login,
                         @Nullable final String password,
                         @Nullable final String email) {
        return null;
    }

    @NotNull
    @Override
    public String getUserId() {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}