package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.endpoint.*;
import ru.tsc.tambovtsev.tm.client.AuthEndpointClient;
import ru.tsc.tambovtsev.tm.client.ProjectEndpointClient;
import ru.tsc.tambovtsev.tm.client.TaskEndpointClient;
import ru.tsc.tambovtsev.tm.client.UserEndpointClient;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    TaskEndpointClient getTaskEndpoint();

    @NotNull
    ProjectEndpointClient getProjectEndpoint();

    @NotNull
    IProjectTaskEndpoint getProjectTaskEndpoint();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    UserEndpointClient getUserEndpoint();

    @NotNull
    AuthEndpointClient getAuthEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    IPropertyService getPropertyService();

}
