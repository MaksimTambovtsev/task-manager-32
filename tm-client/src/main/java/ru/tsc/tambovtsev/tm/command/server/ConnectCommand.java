package ru.tsc.tambovtsev.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.command.AbstractCommand;
import ru.tsc.tambovtsev.tm.enumerated.Role;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @Override
    public @Nullable String getName() {
        return "connect";
    }

    @Override
    public @Nullable String getDescription() {
        return "Connect to server.";
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getServiceLocator().getAuthEndpoint().connect();
        final Socket socket = getServiceLocator().getAuthEndpoint().getSocket();
        getServiceLocator().getProjectEndpoint().setSocket(socket);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return null;
    }

}
