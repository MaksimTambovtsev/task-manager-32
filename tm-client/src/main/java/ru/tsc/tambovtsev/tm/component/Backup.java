package ru.tsc.tambovtsev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.command.data.AbstractDataCommand;
import ru.tsc.tambovtsev.tm.command.data.DataBackupLoadCommand;
import ru.tsc.tambovtsev.tm.command.data.DataBackupSaveCommand;
import ru.tsc.tambovtsev.tm.dto.request.DataBackupLoadRequest;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService es =
            Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        //bootstrap.processCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.getDomainEndpoint().loadDataBackup(new DataBackupLoadRequest());
    }

}
