package ru.tsc.tambovtsev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.client.TaskEndpointClient;
import ru.tsc.tambovtsev.tm.client.UserEndpointClient;
import ru.tsc.tambovtsev.tm.dto.request.UserChangePasswordRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-change-password";

    @NotNull
    public static final String DESCRIPTION = "Change user password.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @Nullable final UserEndpointClient userEndpoint = getServiceLocator().getUserEndpoint();
        userEndpoint.changeUserPassword(new UserChangePasswordRequest(password));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}

