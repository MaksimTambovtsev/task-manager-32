package ru.tsc.tambovtsev.tm.service;

import ru.tsc.tambovtsev.tm.api.service.IDomainService;

public class DomainService implements IDomainService {

    @Override
    public void loadDataBackup() {

    }

    @Override
    public void saveDataBackup() {

    }

    @Override
    public void loadDataBase64() {

    }

    @Override
    public void saveDataBase64() {

    }

    @Override
    public void loadDataBinary() {

    }

    @Override
    public void saveDataBinary() {

    }

    @Override
    public void loadDataJsonFasterXml() {

    }

    @Override
    public void saveDataJsonFasterXml() {

    }

    @Override
    public void loadDataJsonJaxB() {

    }

    @Override
    public void saveDataJsonJaxB() {

    }

    @Override
    public void loadDataXmlFasterXml() {

    }

    @Override
    public void saveDataXmlFasterXml() {

    }

    @Override
    public void loadDataXmlJaxB() {

    }

    @Override
    public void saveDataXmlJaxB() {

    }

    @Override
    public void loadDataYamlFasterXml() {

    }

}
