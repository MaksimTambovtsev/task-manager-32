package ru.tsc.tambovtsev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.endpoint.Operation;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.dto.request.AbstractRequest;
import ru.tsc.tambovtsev.tm.dto.response.AbstractResponse;
import ru.tsc.tambovtsev.tm.task.AbstractServerTask;
import ru.tsc.tambovtsev.tm.task.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class Server {

    @Getter
    @Nullable
    private ServerSocket serverSocket;

    @Getter
    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();

    public Server(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void submit(@NotNull final AbstractServerTask task) {
        executorService.submit(task);
    }

    @SneakyThrows
    public void start() {
        @NotNull final IPropertyService propertyService = bootstrap.getPropertyService();
        @NotNull final Integer port = propertyService.getServerPort();
        serverSocket = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            Class<RQ> reqClass, Operation<RQ, RS> operation
    ) {
        dispatcher.registry(reqClass, operation);
    }

    public Object call(final AbstractRequest request) {
        return dispatcher.call(request);
    }

    @SneakyThrows
    public void stop() {
        if (serverSocket == null) return;
        serverSocket.close();
        executorService.shutdown();
    }

}
