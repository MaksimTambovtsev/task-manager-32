package ru.tsc.tambovtsev.tm.component;

import ru.tsc.tambovtsev.tm.api.endpoint.Operation;
import ru.tsc.tambovtsev.tm.dto.request.AbstractRequest;
import ru.tsc.tambovtsev.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public class Dispatcher {

    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            Class<RQ> reqClass, Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    public Object call(AbstractRequest request) {
        final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
