package ru.tsc.tambovtsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.tambovtsev.tm.api.service.IServiceLocator;
import ru.tsc.tambovtsev.tm.api.service.ITaskService;
import ru.tsc.tambovtsev.tm.dto.request.*;
import ru.tsc.tambovtsev.tm.dto.response.*;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.model.Task;

public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @NotNull
    public TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task = getTaskService().changeTaskStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    public TaskClearResponse clearTask(@NotNull TaskClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getTaskService().clear(userId);
        return new TaskClearResponse();
    }

    @NotNull
    public TaskCreateResponse createTask(@NotNull TaskCreateRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @NotNull
    public TaskListResponse listTask(@NotNull TaskListRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = request.getSortType();
        return new TaskListResponse(getTaskService().findAll(userId,sort));
    }

    @NotNull
    public TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getTaskService().removeById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @NotNull
    public TaskShowByIdResponse showTask(@NotNull TaskShowByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task = getTaskService().findById(userId, id);
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    public TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task = getTaskService().updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

}
